import 'package:flutter/material.dart';
import 'package:widget_package/widget_package.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              // Padding(padding: EdgeInsets.symmetric(horizontal: 10),child: Row(
              //   children: [
              //     SizedBox(
              //       width: MediaQuery.of(context).size.width * 0.2,
              //       child: Row(
              //         children: [
              //           Text("昵称",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Colors.black45),textAlign: TextAlign.center,),
              //         ],
              //       ),
              //     ),
              //     Expanded(child: PasswordInputView(change: (String value){},height: 40,))
              //   ],
              // ),),
              // SizedBox(height: MediaQuery.of(context).size.height * 0.4,child: DatePicker(maxDate: DateTime.now(), onDateSelected: (DateTime date) {
              //   print("===========>${date.toString()}");
              // }, minDate: DateTime.fromMillisecondsSinceEpoch(0),
              //
              // ),)
              SizedBox(
                height: 300,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    children: [
                      Expanded(
                        child: CustomLineChartView(
                            key: UniqueKey(),
                          items: List<CLineItem>.generate(100, (index) => CLineItem((index % 10).toDouble() + 10, "${index % 12 + 1}月")),
                          verticalMaxPoint: 10,
                          horizontalMaxPoint:30,
                          hasDotOnPointOfJunction:true,
                          selectedLabelBuilder: (key,value){
                              return "$key  $value";
                          },
                          selectedColor: Colors.red,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 300,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    children: [
                      Expanded(
                        child: CustomLineChartView(
                          key: UniqueKey(),
                          items: List<CLineItem>.generate(100, (index) => CLineItem((index % 10).toDouble() + 10, "${index % 12 + 1}月")),
                          verticalMaxPoint: 10,
                          horizontalMaxPoint:30,
                          hasDotOnPointOfJunction:true,
                          selectedLabelBuilder: (key,value){
                            return "$key  $value";
                          },
                          selectedColor: Colors.red,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 300,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    children: [
                      Expanded(
                        child: CustomLineChartView(
                          key: UniqueKey(),
                          items: List<CLineItem>.generate(100, (index) => CLineItem((index % 10).toDouble() + 10, "${index % 12 + 1}月")),
                          verticalMaxPoint: 10,
                          horizontalMaxPoint:30,
                          hasDotOnPointOfJunction:true,
                          selectedLabelBuilder: (key,value){
                            return "$key  $value";
                          },
                          selectedColor: Colors.red,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 300,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    children: [
                      Expanded(
                        child: CustomLineChartView(
                          key: UniqueKey(),
                          items: List<CLineItem>.generate(100, (index) => CLineItem((index % 10).toDouble() + 10, "${index % 12 + 1}月")),
                          verticalMaxPoint: 10,
                          horizontalMaxPoint:30,
                          hasDotOnPointOfJunction:true,
                          selectedLabelBuilder: (key,value){
                            return "$key  $value";
                          },
                          selectedColor: Colors.red,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 300,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    children: [
                      Expanded(
                        child: CustomLineChartView(
                          key: UniqueKey(),
                          items: List<CLineItem>.generate(100, (index) => CLineItem((index % 10).toDouble() + 10, "${index % 12 + 1}月")),
                          verticalMaxPoint: 10,
                          horizontalMaxPoint:30,
                          hasDotOnPointOfJunction:true,
                          selectedLabelBuilder: (key,value){
                            return "$key  $value";
                          },
                          selectedColor: Colors.red,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 300,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    children: [
                      Expanded(
                        child: CustomHistogramChartView(
                          items: List<CHistogramItem>.generate(
                            100,
                                (index) => CHistogramItem(index % 10.toDouble() + 10, "$index"),
                          ),
                          horizontalMaxCylinder: 20,
                          verticalMaxScale: 10,
                          labelInterval:5,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: 200,
                height: 200,
                child: CustomPieChartView(
                  items: [
                    CPieItem(100, "食品", Colors.red),
                    CPieItem(1000, "衣服", Colors.blue),
                    CPieItem(2000, "电子产品", Colors.green),
                    CPieItem(300, "其他", Colors.pink),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
